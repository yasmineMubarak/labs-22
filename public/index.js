function windowScroll() {
    const navbar = document.getElementById("navbar");
    const mainContent = document.getElementById("main-content");
    if (
        document.body.scrollTop >= 50 ||
        document.documentElement.scrollTop >= 50
    ) {
        navbar.classList.add("nav-sticky");
        
    } else {
        navbar.classList.remove("nav-sticky");
        
    }
}
window.addEventListener("scroll", (ev) => {
    ev.preventDefault();
    windowScroll();
});


// function changetextfunction(){
//     var changetext = document.getElementById("change-text").innerHTML;
//     if (changetext == "AUTOMATION"){
//         document.getElementById("change-text").innerHTML="INNOVATION";
//     }
//     else{
//         document.getElementById("change-text").innerHTML="AUTOMATION";
//     }
// }
// setInterval(changetextfunction,1000);